import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from tqdm.notebook import tqdm
import shutil


import ipywidgets as widgets
from ipywidgets import  interactive

# Jupyter/ IPython formatting
from IPython.display import Math, Latex, display
LegendFontSize=20
LabelFontSize= 20
TickFontSize= 15

# Default values for plots
plt.rcParams["figure.figsize"] = [14, 9] # figure width and height
plt.rcParams["font.size"] = 20

# Data loading, encapsulated to make it less installation and OS dependant
import os.path
from zipfile import ZipFile
def AugerOpen(fdir, file):
    """
    Loads a file from the auger open data release. Can be either in the local directory,
    in the parent directory or in the augeropendata directory.
    File is identified by it directory *fdir* and filename *file* and can be found in the directory
    or in a zip file.
    """
    for loc in [".", "..", "augeropendata", "data"]:
        fname = os.path.join(loc, fdir, file)
        if os.path.isfile(fname):
            return open(fname)
        zname=os.path.join(loc, fdir + ".zip")
        if os.path.isfile(zname):
            with ZipFile(zname) as myzip:
                return myzip.open(os.path.join(fdir, file))
    raise FileNotFoundError(os.path.join(fdir, file))

data = pd.read_csv(AugerOpen('.', 'dataSummary.csv'))
data['lgE'] = np.log10(data['fd_totalEnergy']) + 18 # units: lg(E/eV)

# Does removing a majority of the points here have any significance later in code
xmax_data = data[(data.fd_hdXmaxEye == 1)].copy() # copy so we can add columns later 

grouped_xmax_data = xmax_data.groupby('id')

unique_xmax_data = xmax_data.drop_duplicates('id').set_index('id')

n_events = len(xmax_data)
n_unique = len(unique_xmax_data)

#display(Latex(f'''Read {n_events} Xmax events of which {n_unique} are unique.
    #The total number of FD events (counting multi-eye events as multiple events) is {n_events}.'''))


## Calculate weights: w = 1/uncertainty^2
xmax_data['fd_e_weight'] = 1 / np.square(xmax_data.fd_dtotalEnergy)
xmax_data['fd_xmax_weight'] = 1 / np.square(xmax_data.fd_dxmax)
## Calculate value * w
xmax_data['fd_e_weighted'] = xmax_data.fd_totalEnergy * xmax_data.fd_e_weight
xmax_data['fd_xmax_weighted'] = xmax_data.fd_xmax * xmax_data.fd_xmax_weight

# average of energies
sum_of_e_weights = grouped_xmax_data['fd_e_weight'].sum()
fd_avg_e = grouped_xmax_data['fd_e_weighted'].sum() / sum_of_e_weights
unique_xmax_data['fd_avg_lgE'] = np.log10(fd_avg_e*1e18)
fd_davg_e = 1 / np.sqrt(sum_of_e_weights)
fd_davg_lge = fd_davg_e / fd_avg_e / np.log(10.)
unique_xmax_data['fd_davg_lgE'] = fd_davg_lge

# average of Xmax
sum_of_xmax_weights = grouped_xmax_data['fd_xmax_weight'].sum()
fd_avg_xmax = grouped_xmax_data['fd_xmax_weighted'].sum() / sum_of_xmax_weights
unique_xmax_data['fd_avg_xmax'] = fd_avg_xmax
fd_davg_xmax = 1 / np.sqrt(sum_of_xmax_weights)
unique_xmax_data['fd_davg_xmax'] = fd_davg_xmax

# average of Galatic Latitude
fd_avg_b = grouped_xmax_data['fd_b'].mean()
unique_xmax_data['fd_avg_b'] = fd_avg_b

# average of Gal Longitude
fd_avg_l = grouped_xmax_data['fd_l'].mean()
unique_xmax_data['fd_avg_l'] = fd_avg_l

def ElongationFunction(lgE, p):
    x = lgE
    Rate = p[0] + p[1]*(x-18.) + p[2]*(x-18.)*(x-18.)
    return Rate

Model = [648.6042870553292, 63.12370268046462, -1.971476995005652] #EPOS LHC Fe

def AngularDistance(l1,b1,l2,b2): #Angular separation between two points in l,b
    l1_rad = np.deg2rad(l1)
    b1_rad = np.deg2rad(b1)
    l2_rad = np.deg2rad(l2)
    b2_rad = np.deg2rad(b2)
    return np.arccos(np.sin(b1_rad) * np.sin(b2_rad) + np.cos(b1_rad) * np.cos(b2_rad) * np.cos(l1_rad - l2_rad))

def InTopHat(event_l, event_b, bin_l, bin_b, opening_angle):
    in_window = False
    opening_angle_rad = np.deg2rad(opening_angle)
    ang_dist = AngularDistance(bin_l, bin_b, event_l, event_b)

    if ang_dist <= opening_angle_rad:
        in_window = True

    return in_window

par = Model
unique_xmax_data['xmax_norm'] = unique_xmax_data.fd_avg_xmax - ElongationFunction(unique_xmax_data.fd_avg_lgE, par)

#for ind in unique_xmax_data.index:
#     print(unique_xmax_data['fd_avg_lgE'][ind], unique_xmax_data['fd_avg_xmax'][ind], unique_xmax_data['xmax_norm'][ind])

fig = plt.figure(figsize=(16, 8))

x = np.arange(-180.1,180.1, 2)
x =np.deg2rad(x)
y = np.arange(-90, 90.1, 2)
y = np.deg2rad(y)
z = np.zeros((len(y),len(x)))

xx, yy = np.meshgrid(x, y)

emin = 18.7
emax = 21
conversion_time = 86400

en = unique_xmax_data.fd_avg_lgE[unique_xmax_data.fd_avg_lgE.between(emin, emax)].values
xmax = unique_xmax_data.fd_avg_xmax[unique_xmax_data.fd_avg_lgE.between(emin, emax)].values
event_tim = unique_xmax_data.gpstime[unique_xmax_data.fd_avg_lgE.between(emin, emax)].values
event_tim = np.int0(np.trunc(event_tim/conversion_time))
event_tim = event_tim - event_tim[0]
frame_cnt = event_tim[-1]
print(event_tim)

lat = unique_xmax_data.fd_avg_b[unique_xmax_data.fd_avg_lgE.between(emin, emax)].values
longit = unique_xmax_data.fd_avg_l[unique_xmax_data.fd_avg_lgE.between(emin, emax)].values

long = []
for lon in longit:
    if lon < 180:
        long.append(lon)
    else:
        long.append(lon - 360)

includevar = 0
previous_state_copy = 0

xmax_min = min(xmax)
xmax_max = max(xmax)
xmax_adj = (xmax_max-xmax_min)/10
xmax_adj_val = xmax/xmax_adj
xmax_adj_val = xmax_adj_val-(xmax_max/xmax_adj)+5

for k in range(0,frame_cnt+1):
    print(k, "/", frame_cnt)
    search_con = 1
    increased = 0
    while search_con == 1: 
        if k == frame_cnt:
            search_con = 0
        if event_tim[includevar] <= k:
            includevar += 1
            increased = 1
        else:
            search_con = 0
    #print(increased,includevar,k,event_tim[includevar])
    event_k = en[0:includevar]
    if increased == 1 or previous_state_copy == 0:
        for i in range(0, len(y)):
            bin_b = np.rad2deg(y[i])
            # bin_b = y[i]
            for j in range(0, len(x)):
                bin_l = np.rad2deg(x[j])
                # bin_l = x[j]
                xmax_in = []
                xmax_out = []
                for event in range(0, len(event_k)):
                    event_b = lat[event]
                    event_l = long[event]
                    event_xmax = xmax[event]
                    # print(event_b, event_l, event_xmax)
                    if InTopHat(event_l, event_b, bin_l, bin_b,30):
                        xmax_in.append(xmax[event])
                    else:
                        xmax_out.append(xmax[event])

                if len(xmax_in) == 0:
                    mean_in = 0
                    std_in = 0
                else:
                    mean_in = np.mean(xmax_in)
                    std_in = np.std(xmax_in)/np.sqrt(len(xmax_in))
                if len(xmax_out) == 0:
                    mean_out = 0
                    std_out = 0
                else:
                    mean_out = np.mean(xmax_out)
                    std_out = np.std(xmax_out)/np.sqrt(len(xmax_out))
                if np.sqrt(std_in**2 + std_out**2) != 0:
                    z[i,j] = (mean_in - mean_out)/np.sqrt(std_in**2 + std_out**2)
                if np.isnan(z[i,j]):
                    z[i,j] = 0
                if len(xmax_in) < 4:
                    z[i,j] = 0
        new_events_loc = np.where((event_tim > k - 45) & (event_tim <= k))[0]
        if len(new_events_loc) != 0:
            event_loc_min = min(new_events_loc)
            event_loc_max = max(new_events_loc)
        else:
            event_loc_min = 0
            event_loc_max = 0
        event_lat = []
        event_long = []
        event_xmax = []
        event_size = []
        for ev_i in range(event_loc_min,event_loc_max+1):
            event_size_k = (50)-(k-event_tim[ev_i])            
            if event_size_k > 0: 
                event_lat.append(lat[ev_i])
                event_long.append(long[ev_i])
                event_xmax.append(xmax_adj_val[ev_i])
                event_size.append(event_size_k)
        #plt.scatter(event_long,event_lat,color='black',s=30)
        ax = fig.add_subplot(111, projection="hammer")
        hm = ax.pcolormesh(xx, yy, z, shading='auto', cmap='bwr',vmin = -5, vmax = 5)
        hmsc = ax.scatter(np.deg2rad(event_long),np.deg2rad(event_lat),c=event_xmax,cmap='bwr',vmin=-5,vmax=5,s=event_size,edgecolors='Black')
        plt.grid(True)
        cbar = fig.colorbar(hm)

        cbar.set_label('Heavier  <----  Composition  ---->  Lighter', size=14)
        fig.savefig('Frames/test_{}.png'.format(k))
        fig.clear()
        if len(new_events_loc) == 0:
            previous_state_copy = 1
        else:
            previous_state_copy = 0 
    else:
        shutil.copy('Frames/test_{}.png'.format(k-1),'Frames/test_{}.png'.format(k))
        previous_state_copy = 1

print("Done Generating Frames")
print("Use Intructions in /Frames to create Movie")